## What’s changed

## 🐛 Bug fixes

- 🚑 Fix InfluxDB start by adding six as dependency @ferrix (#299)

## ⬆️ Dependency updates

- ⬆️ Upgrades python3 to 3.10.8-r0 @frenck (#300)
- ⬆️ Upgrades add-on base image to 12.2.7 @frenck (#301)
